# Countdowns

Webapp for creating countdowns.

User data persists through sessions in localStorage.

Built with Vue.

App is available at https://russofrancesco.gitlab.io/countdowns .