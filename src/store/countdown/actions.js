export default {
    addCountdown(context, payload)
    {
        console.log("action: addCountdown");
        context.commit('addCountdownToState', payload);
        context.commit('saveCountdownsToLS');
    },
    deleteCountdown(context, payload)
    {
        context.commit('deleteCountdownFromState', payload);
        context.commit('saveCountdownsToLS');
    },
    loadCountdowns(context)
    {
        console.log("action: loadCountdowns");
        context.commit('loadCountdownsFromLS');
    },
}
