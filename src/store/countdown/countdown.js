export function Countdown(title, descr, end_timestamp)
{
    this.title = title;
    this.descr = descr;
    this.end_timestamp = end_timestamp;
    this.time_left = null;
}
