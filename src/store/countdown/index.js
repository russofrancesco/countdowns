import countdownGetters from '@/store/countdown/getters.js';
import countdownMutations from '@/store/countdown/mutations.js';
import countdownActions from '@/store/countdown/actions.js';
import { Countdown } from '@/store/countdown/countdown.js';

const state = {
    countdowns: [],
    localStorage_key: 'countdowns',
}

export default {
    namespaced: true,
    state,
    getters: countdownGetters,
    mutations: countdownMutations,
    actions: countdownActions,
};
