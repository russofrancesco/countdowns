import { Countdown } from '@/store/countdown/countdown.js';

export default {
    addCountdownToState(state, payload)
    {
        console.log('addCountdownToState: ', payload);
        const new_countdown = new Countdown(
            payload.title,
            payload.descr,
            payload.end_timestamp
        );
        state.countdowns.push(new_countdown);
    },
    deleteCountdownFromState(state, payload)
    {
        const idx = state.countdowns.indexOf(payload);
        console.log(`mutation: deleting ${state.countdowns[idx].title}`);
        state.countdowns.splice(idx, 1); // Remove 1 item at index idx
    },
    saveCountdownsToLS(state)
    {
        const countdowns_as_string = JSON.stringify(state.countdowns);
        console.log("mutation saveCountdownsToLS: " + countdowns_as_string);
        localStorage.setItem(state.localStorage_key, countdowns_as_string);
    },
    loadCountdownsFromLS(state)
    {
        const countdowns_as_string = localStorage.getItem(state.localStorage_key);
        state.countdowns = JSON.parse(countdowns_as_string);
        if(!state.countdowns)
            state.countdowns = [];
    },
}
