import { createStore } from 'vuex';
import countdownModule from '@/store/countdown/index.js';

const modules = {countdown: countdownModule};

const store = createStore(
{
    modules,
    state:
    {
    },
    getters:
    {
    },
    mutations:
    {
    },
    actions:
    {
    },
});

export default store;
