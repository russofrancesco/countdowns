const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  publicPath: process.env.NODE_ENV === 'production' ?
                '/' + process.env.CI_PROJECT_NAME + '/' : '/',
  transpileDependencies: true,
  css: {
      loaderOptions: {
          scss: {
              additionalData: `@import "@/styles/theme.scss";`
          },
      },
  },
})
